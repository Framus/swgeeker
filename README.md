# Star-Wars Geeker

Welcome to Star Wars Geeker. You found my little space to test out technologies, concepts and methods.
Since it is always a good thing to have some use case to test out things, I created this little scenario.

# Implementations

Each implementation contains its own README.md to illustrate the setup and document the outcome and lessons learned
* [Quarkus CLI](./quarkus/README.md)

# The Scenario

To Structure everything a little bit, the scenario is written in stories (you know that agile requirement thingy ;) )

### Story1 - Show Off
As a Star Wars Geek I want to find an store my loved characters of the Star Wars universe from swapi.dev , so that I can show off my Star Wars know how

#### Details:
- Beside the name, also the home world should be visible
- I also want to access my characters if swapi.dev is currently offline

### Story2: Choose a side
As a Star Wars Geek I want to add the side of the force (dark, light, neutral) my character is on, since swapi.dev does not give that information.

### Story3: Rate Me
Als Star Wars Enthusiast\*in will ich meinen Lieblingscharaktere zusätzlich eine 4-Sterne Bewertung geben, damit ich sie priorisieren kann.

### Story4: Share Me
Als Star Wars Enthusiast\*in möchte ich eine Auswertung sehen welche Charaktere von wievielen anderen Star Wars Enthusiast\*in als Lieblingscharakter angegeben wurden.

### Story5: Measure Me
Als Star Wars Enthusiast\*in möchte ich eine Auswertung über die durchschnittliche Sternebewertung aller Charaktere in einer Top-Liste sehen, damit ich sehe welcher Charakter derzeit der beliebteste ist
